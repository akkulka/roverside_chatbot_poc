FROM ubuntu:latest
LABEL maintainer Akshay Kulkarni "akshay@ozz.ai"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential git
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["application.py"]