from google.cloud import bigquery

client = bigquery.Client.from_service_account_json('roverside_bigquery.json')
print(client)
# query_job = client.query("""
#     #standardSQL
#     SELECT state_name FROM `bigquery-public-data.epa_historical_air_quality.co_daily_summary` where aqi = 
#     (select max(aqi) from `bigquery-public-data.epa_historical_air_quality.co_daily_summary`)""")

# results = query_job.result()
# print(type(results))

# for row in results:
#     print(row.state_name)



def find_max_aqi_trend(state_name, days=10):
    state_name = state_name.title()
    state_aqi_trend_query = (""" SELECT max(aqi) as max_aqi,date_local FROM `bigquery-public-data.epa_historical_air_quality.co_daily_summary` 
    where state_name = '{}' and aqi is not NULL
    group by date_local
    order by date_local DESC 
    LIMIT {} """.format(state_name,days))

    query_job = client.query(state_aqi_trend_query)
    results = query_job.result()

    for row in results:
        print("{} : {}".format(row.date_local,row.max_aqi))


find_max_aqi_trend('new york',3)


# query_job = client.query(state_aqi_trend_query)
# results = query_job.result()

# for row in results:
#     print("{} : {}".format(row.date_local,row.max_aqi))