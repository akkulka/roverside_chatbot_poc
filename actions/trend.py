from google.cloud import bigquery
from chatbase import Message
import os
import json 

from pymongo import MongoClient

client = MongoClient('mongodb://akshay:akshay@ds261678.mlab.com:61678/roverside')
db = client['roverside']
mappings = db.mappings
failures = db.failures

def runQuery(session):
    intent = session["intent"]
    message = session["message"]
    print(intent)
    entities = session["entities"]
    creds = os.listdir(os.path.join(os.getcwd(),'credentials'))

    map = mappings.find_one({'user':'akshay','name':intent})

    msg = Message(api_key="57587134-4d16-4ebe-b06b-372d4e1afc23",
              platform="roverside",
              version="0.1",
              user_id=session['user']['name'],
              message=message,
              intent=intent)
    resp = msg.send()
    

    if session['user']['name'] + '.json' in creds:
        client = bigquery.Client.from_service_account_json(os.path.join(os.getcwd(),'credentials',session['user']['name'] + '.json'))
    else:
        return {"response": "Sorry couldn't authenticate you","type":"text"}
    
    
    if 'date-period' in entities and entities['date-period'] != '':
        entities['fromDate'], entities['toDate'] = entities['date-period'].split("/")
        start_date = entities['fromDate'].split('-')[-1]
        end_date = entities['toDate'].split('-')[-1]
        entities["days"] = int(end_date) - int(start_date)
    if 'geo-city' in entities and 'geo-state-us' in entities and entities['geo-city'] != '' and entities['geo-state-us'] == '':
        entities['geo-state-us'] = entities['geo-city']
    if 'geo-state-us' in entities and 'geo-city' in entities and entities['geo-state-us'] != '' and entities['geo-city'] == '':
        entities['geo-city'] = entities['geo-state-us']
    
    
    if map:
        sql = map['sql']
        entities_needed = [word for word in sql.split(' ') if word.startswith('@') or word.startswith("'@")]
        for entity in entities_needed:
            if entity[0] == "'":
                entity = entity[1:-1]
                ent_name = entity[1:]
            else:
                ent_name = entity[1:]
            if ent_name in entities and entities[ent_name] != '':
                sql = sql.replace(entity,str(entities[ent_name]))
            else:
                default_val = map['default_values'][ent_name]
                sql = sql.replace(entity,str(default_val))
            
        # print(sql)
        query_job = client.query(sql)
        results = query_job.result()
        # print(results)
        response_list = []
        response_format = map["result"]
        

        i = 0
        for row in results:
            i += 1
            # print(row)
            response_obj = {}
            for item in response_format:
                response_obj[item] = row[response_format[item]]
            response_list.append(response_obj)

        if i == 0:
            msg = Message(api_key="57587134-4d16-4ebe-b06b-372d4e1afc23",
              platform="roverside",
              version="0.1",
              user_id="agent",
              message="Sorry couldn't find results",
              intent=intent)
            resp = msg.send()
            return {"response":"Sorry couldn't find results","type":"text","intent":intent,"entities":entities,"message":session['message']}
        msg = Message(api_key="57587134-4d16-4ebe-b06b-372d4e1afc23",
              platform="roverside",
              version="0.1",
              user_id="agent",
              message="graph response",
              intent=intent)
        resp = msg.send()
        return {"response":response_list,"type":"graph","graph":map['graph'],"intent":intent,"entities":entities,"message":session['message']}
    else:
        fail_obj = {"message":session['message'],"intent":intent,"entities":entities}
        failures.insert_one(fail_obj)
        msg = Message(api_key="57587134-4d16-4ebe-b06b-372d4e1afc23",
              platform="roverside",
              version="0.1",
              user_id="agent",
              message="Sorry I couldn't understand",
              intent='None')
        resp = msg.send()
        return {"response":"Sorry I couldn't understand", "type":"text"}
    
    
    # if current_map_name in maps:
    #     data = json.load(open(os.path.join(os.getcwd(),'mappings',current_map_name)))
    #     if intent in data:
    #         sql = data[intent]['sql'] 
    #         entities_needed = [word for word in sql.split(' ') if word.startswith('@') or word.startswith("'@")]
    #         for entity in entities_needed:
    #             if entity[0] == "'":
    #                 entity = entity[1:-1]
    #                 ent_name = entity[1:]
    #             else:
    #                 ent_name = entity[1:]
    #             if ent_name in entities and entities[ent_name] != '':
    #                 sql = sql.replace(entity,str(entities[ent_name]))
    #             else:
    #                 default_val = data[intent]['default_values'][ent_name]
    #                 sql = sql.replace(entity,str(default_val))
            
    #         #print(sql)
    #         query_job = client.query(sql)
    #         results = query_job.result()

    #         response_list = []

    #         response_format = data[intent]["result"]
            

    #         i = 0
    #         for row in results:
    #             i += 1
    #             # print(row)
    #             response_obj = {}
    #             for item in response_format:
    #                 response_obj[item] = row[response_format[item]]
    #             response_list.append(response_obj)

    #         if i == 0:
    #             return {"response":"Sorry couldn't find results","type":"text"}
            
    #         return {"response":response_list,"type":"graph","graph":data[intent]['graph']}
    #     else:
    #         fail_data = json.load(open(os.path.join(os.getcwd(),'failures',current_map_name)))
    #         if "fails" in data:
    #             fail_data["fails"].append(session['message'])
    #         else:
    #             fail_data["fails"] = [session['message']]
            
    #         with open(os.path.join(os.getcwd(),'failures',current_map_name),'w') as outFile:
    #             json.dump(fail_data, outFile)
    #         return {"response":"Sorry I couldn't understand", "type":"text"}
    return {"response":"Running the query","type":"text"}




# creds = os.listdir(os.path.join(os.getcwd(),'credentials'))
# clients = {}

# for cred in creds:
#     cred_name = cred[:-5]
#     clients[cred_name] = bigquery.Client.from_service_account_json(os.path.join(os.getcwd(),'credentials',cred))

# print(clients)
# client = bigquery.Client.from_service_account_json('roverside_bigquery.json')

# def find_max_aqi_trend(session,state_name="Arizona", days=10):
#     entities = session["entities"]
#     user_name = session['user']['name']
#     print(user_name)

#     if 'date-period' in entities and entities['date-period'] != '':
#         start, end = entities['date-period'].split("/")
#         start_date = start.split('-')[-1]
#         end_date = end.split('-')[-1]
#         days = int(end_date) - int(start_date)
    
#     if 'geo-city' in entities and entities['geo-city'] != '':
#         entities['geo-state-us'] = entities['geo-city']
#     if 'geo-state-us' in entities and entities['geo-state-us'] != '':
#         state_name = entities['geo-state-us']
#         print(state_name)
#         state_name = state_name.title()

#         if 'qualifier' in entities and entities['qualifier'] != '':
#             if entities['qualifier'] == 'max':
#                 state_aqi_trend_query = (""" SELECT max(aqi) as val FROM `bigquery-public-data.epa_historical_air_quality.co_daily_summary` 
#                 where state_name = '{}' and aqi is not NULL""".format(state_name))
#             else:
#                 state_aqi_trend_query = (""" SELECT min(aqi) as val FROM `bigquery-public-data.epa_historical_air_quality.co_daily_summary` 
#                 where state_name = '{}' and aqi is not NULL""".format(state_name))

#             query_job = client.query(state_aqi_trend_query)
#             results = query_job.result()

#             i = 0
#             for row in results:
#                 i += 1
#                 return {"response":"The " + entities['qualifier'] + " air quality index for " + state_name + " is " + str(row.val), "type":"text"}
        
#             if i == 0:
#                 return {"response":"Sorry couldn't find results","type":"text"}
            
#         else:
#             state_aqi_trend_query = (""" SELECT state_name, max(aqi) as max_aqi,date_local FROM `bigquery-public-data.epa_historical_air_quality.co_daily_summary` 
#             where state_name = '{}' and aqi is not NULL
#             group by date_local, state_name
#             order by date_local DESC 
#             LIMIT {} """.format(state_name,days))

#             query_job = client.query(state_aqi_trend_query)
#             results = query_job.result()

#             response_list = []

#             i = 0
#             for row in results:
#                 i += 1
#                 response_obj = {"state_name":row.state_name,"date":row.date_local,"aqi":row.max_aqi}
#                 response_list.append(response_obj)

#             if i == 0:
#                 return {"response":"Sorry couldn't find results","type":"text"}
            
#             return {"response":response_list,"type":"graph"}
#     else:
#         return {"responses":"Please mention the state name inside your query","type":"text"}