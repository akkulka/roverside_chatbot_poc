from flask import Flask, request, render_template, redirect, send_file, jsonify
from flask_wizard import Wizard
from flask_basicauth import BasicAuth
from flask_cors import CORS
from google.cloud import bigquery

import os
import json
import pymongo
import http.client
import requests

from pymongo import MongoClient

application = Flask(__name__)
wizard = Wizard(application)
CORS(application)

path = os.path.join(os.getcwd(),'config.json')
with open(path,"r") as jsonFile:
    data = json.load(jsonFile)
client = MongoClient(data['mongo']['mongo_uri'])

application.config['BASIC_AUTH_USERNAME'] = 'roverside_poc'
application.config['BASIC_AUTH_PASSWORD'] = 'Seoul123!'

basic_auth = BasicAuth(application)
db = client['roverside']

@application.route("/")
def index():
    creds = os.listdir('./credentials')
    users = []
    for cred in creds:
        users.append(cred[:-5])
    return render_template('index.html',users=users)

@application.route("/failures",methods=['GET','POST'])
def failures():
    if request.method == 'GET':
        fail_c = db.failures
        fail_list = fail_c.find()
        fails = []
        for fail in fail_list:
            fails.append({"message":fail['message'],"intent":fail['intent']})
        return render_template('failures.html',fails=fails)
    if request.method == 'POST':
        value = request.get_json()
        failures = db.failures
        fail_obj = {"message":value["query"],"intent":value["intent"],"entities":value["entities"]}
        failures.insert_one(fail_obj)
        return "success"


@application.route("/pinned",methods=["GET","POST"])
def pinned():
    if request.method == 'GET':
        pins = db.pinned
        pin_iter = pins.find()
        pin_list = []
        for pin in pin_iter:
            pin_list.append(pin["message"])
        return jsonify({"pins":pin_list})
    if request.method == 'POST':
        value = request.get_json()
        pins = db.pinned
        pin_obj = {"user":"akshay","message":value["query"]}
        pins.insert_one(pin_obj)
        return "success"

@application.route("/entities", methods=["GET","POST"])
def entities():
    url = "https://api.dialogflow.com/v1/entities"

    querystring = {"v":"20150910"}

    headers = {
        'Authorization': "Bearer 3f88e72a203847a186cf088935635bd0",
        'Cache-Control': "no-cache",
        'Postman-Token': "75b4c0a9-c68d-5328-46fc-0b6dac88aed9"
        }

    response = requests.request("GET", url, headers=headers, params=querystring)

    ents = []
    result = json.loads(response.text)
    for ent in result:
        id = ent['id']
        name = ent['name']
        url = "https://api.dialogflow.com/v1/entities/" + id

        resp = requests.request("GET", url, headers=headers, params=querystring)

        res = json.loads(resp.text)
        values = res['entries']
        ents.append({"id":id,"name":name,"values":values})

    return jsonify(ents)



@application.route("/dashboard")
def dashboard():
    client = bigquery.Client.from_service_account_json(os.path.join(os.getcwd(),'credentials', 'akshay.json'))
    dsets = []
    datasets = client.list_datasets()
    for dataset in datasets:
        dsets.append(dataset._dataset_id)
    return render_template('dashboard.html', dsets=dsets)    

@application.route("/add", methods=["GET","POST"])
def add():
    if request.method == 'GET':
        creds = os.listdir('./credentials')
        users = []
        for cred in creds:
            users.append(cred[:-5])
        return render_template('add.html', users=users)

    if request.method == 'POST':
        filename = request.form['username'] + '.json'
        file = request.files['credfile']
        file.save('./credentials/'+ filename)

        project_mappings = {}
        with open(os.path.join(os.getcwd(),'mappings',filename),'w') as outFile:
            json.dump(project_mappings, outFile)

        failures = {}
        with open(os.path.join(os.getcwd(),'failures',filename),'w') as outFile:
            json.dump(failures, outFile)
        return redirect('/')


@application.route("/mappings", methods=["GET","POST"])
def ent():
    if request.method == 'GET':
        maps = os.listdir('./mappings')
        return render_template('mapping.html', maps = maps)

    if request.method == 'POST':
        file_name = request.files["upload"].filename
        file = request.files["upload"]
        file.save('./mappings/'+file_name)
        return redirect('/mappings')


@application.route("/map/<file_name>", methods=["GET"])
def download(file_name):
    if request.method == 'GET':
        return send_file(os.path.join(os.getcwd(),'mappings',file_name),attachment_filename=file_name)

if __name__ == '__main__':
	application.run(port=8080)